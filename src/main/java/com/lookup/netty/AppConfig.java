package com.lookup.netty;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@EnableWebMvc
@PropertySource(value = "classpath:datasource.properties")
@MapperScan("com.lookup.buss.sqlmapper")
@ComponentScan("com.lookup")
@EnableCaching//标注启动缓存.

  public class AppConfig {


    @Value("${db.url}")
    public String jdbcUrl;

    @Value("${db.username}")
    public String username;
    @Value("${db.password}")
    public String password;
    @Value("${db.driverClassName}")
    public String driverClassName;
    @Value("${db.connectionTestQuery}")
    public String connectionTestQuery;

    @Value("${db.connectionTimeout}")
    public String connectionTimeout;
    @Value("${db.idleTimeout}")
    public String idleTimeout;
    @Value("${db.maxLifetime}")
    public String maxLifetime;

    @Value("${db.maximumPoolSize}")
    public String maximumPoolSize;
    @Value("${db.minimumIdle}")
    public String minimumIdle;
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    @Bean(name = "mysql")
    public DataSource buildDataSource() {

        com.zaxxer.hikari.HikariDataSource datasource = new com.zaxxer.hikari.HikariDataSource();
        datasource.setJdbcUrl(jdbcUrl);
        datasource.setDriverClassName(driverClassName);
        datasource.setUsername(username);
        datasource.setPassword(password);
        datasource.setMaximumPoolSize(Integer.valueOf(maximumPoolSize));
        return datasource;
    }

    @Bean
    public SqlSessionFactory buildsqlSessionFactory() throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(buildDataSource());

        //添加插件
        bean.setPlugins(new Interceptor[]{});

       ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
       bean.setMapperLocations(resolver.getResources("classpath*:com/lookup/buss/xml/*.xml"));
        return bean.getObject();
    }

    @Bean
    public EhCacheManagerFactoryBean buildFactory() throws IOException {
        EhCacheManagerFactoryBean cacheManagerFactoryBean  = new EhCacheManagerFactoryBean();
        cacheManagerFactoryBean.setShared(true);
        return cacheManagerFactoryBean;
    }
    @Bean
    public EhCacheCacheManager buildCache() throws IOException {
        EhCacheCacheManager manager  = new EhCacheCacheManager();
        manager.setCacheManager(buildFactory().getObject());
        return manager;
    }

}
