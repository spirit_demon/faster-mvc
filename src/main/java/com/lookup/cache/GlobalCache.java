package com.lookup.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class GlobalCache {
    private Logger logger  = LoggerFactory.getLogger(this.getClass());

    @Cacheable(cacheNames = "default")
    public  String getCache (String key )
    {
        return key ;
    }
}
