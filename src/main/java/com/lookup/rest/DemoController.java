package com.lookup.rest;


import com.lookup.buss.sqlmapper.MysqlServiceMapper;
import com.lookup.cache.GlobalCache;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.inject.Inject;
import java.util.Date;
import java.util.HashMap;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @Inject
    MysqlServiceMapper mysqlServiceMapper;
    @Inject
    GlobalCache globalCache;



    @RequestMapping(value="/now",method={RequestMethod.GET , RequestMethod.POST})
    public ModelAndView now(){
         //数据库查询
        //String now=  mysqlServiceMapper.getNow();
        String now=  new Date().toString();
        globalCache.getCache("1322321");
        ModelAndView view  = new ModelAndView("index");
        view.addObject("title","模板");
        view.addObject("now",now);
        return view;
    }


    @RequestMapping(value="/hello",method={RequestMethod.GET , RequestMethod.POST})
    @ResponseBody
    public HashMap<String ,Object> json(@RequestParam(value = "hello",required = false) String hello
                            ) {
        HashMap<String,Object>  hashMap  = new HashMap<>();
        hashMap.put("user","你好");
        hashMap.put("age",18);

        return hashMap;

    }
}
