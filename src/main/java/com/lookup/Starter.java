package com.lookup;
import com.lookup.netty.MyServer;

public class Starter {

	public static void main(final String[] args) throws Exception {
        int port;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        } else {
            port = 9000;
        }
        new MyServer(port).run();
    }
}
