# fast-mvc 比springboot更易用上手的mvc框架

### 特点
1. 基于springmvc轻量级解决方案，比SpringBoot更简单上手
2. 不依赖servlet 容器，底层IO框架基于netty
3. 基于spring annotation 配置，完美继承spring特性，易扩展
4. 数据库、缓存、模版引擎，静态资源(js,css,img) 应有竟有
5. 学习成本低，文档简单


### 哪些人需要这个框架
1  中小企业的api网关服务器 
2  中小企业的运营后台框架



### 应用
1. clone git工程 
2. 启动配置数据库，默认配置mysql，支持所有数据库
3. 修改datasource.properties 配置，支持其他数据库需要额外增加jdbc驱动
4. Run Starter 



### 写在最后
  当前这套框架原本是公司用来解决摆脱tomcat容器和提高io性能,倒腾的一套框架，作为后端应用服务器目前运行稳定，springboot的出现，fast-mvc估计只能作为学习研究的范例，和springboot目标一致，springboot是更优秀的框架，有专门的组织维护，生态更加健全，后端技术选型首选springboot。
hhhhhhjhnbcncn






khkjhkhkkk
  